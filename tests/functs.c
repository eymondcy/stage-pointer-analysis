#include <stdio.h>

void f1();
void f2();
void f3();
void f4();
void f5();
void f6();
void f7();

int main() {
	int a = 6, b = 5, c = 10;
	//int *ptr = &a;
    f1();
    f2();
    f3(45);
    f4(8);
    f5();
    f6();
    f7();
	b = a * c;
	//printf("%d%d%d%d\n", *ptr, a, b, c);
    return 0;
}

void f1() {
    printf("Je suis f1\n");
}

void f2() {
    printf("Je suis f2\n");
}

void f3(int a) {
	if(a >= 16) {
    	printf("Je suis f3\n");
	} else {
		printf("Je suis f3, et on a pris le else\n");
	}
}

void f4(int b) {
    printf("Je suis f4 ! Et voilà %d\n", b);
}

void f5() {
    printf("Je suis f5\n");
}

void f6() {
    printf("Je suis f6\n");
}

void f7() {
    printf("Je suis f7\n");
}

