//===- Hello.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/WPA/WPAPass.h"
#include "llvm/WPA/Andersen.h"
#include "llvm/MemoryModel/PointerAnalysis.h"
#include "llvm/MemoryModel/PAGBuilder.h"
#include "llvm/WPA/FlowSensitive.h"
#include "llvm/Util/SVFModule.h"
#include "llvm/ADT/SparseBitVector.h"
#include "llvm/MemoryModel/PointsToDFDS.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include <algorithm>

using namespace llvm;

#define DEBUG_TYPE "AndersenItf"

namespace {
  // Hello - The first implementation, without getAnalysisUsage.
  struct AndersenItf : public FunctionPass {
    static char ID; // Pass identification, replacement for typeid
    AndersenItf() : FunctionPass(ID) {}

    // void printPointToSet(std::vector<const llvm::Value*> *set, AndersenAAResult * AA){
    //   for(int i = 0; i<set->size(); i++){
    //     errs() <<" "<<set->at(i)<<"\n";
    //     std::vector<const llvm::Value *> temp;
    //   }
    // }

    bool runOnFunction(Function &F) override {
      SVFModule svfModule(F.getParent());
      // getAnalysis<WPAPass>().runOnModule(svfModule);
      // //WPAPass *wpa;
      // //wpa->runOnModule(svfModule);
      // WPAPass::PTAVector* ptaVector = &getAnalysis<WPAPass>().returnPointerAnalysis();
      // BVDataPTAImpl::PTDataTy *ptd;
      // BVDataPTAImpl::IncDFPTDataTy *incDataTy;
      // PointerAnalysis *pa = ptaVector->at(0);
      // FlowSensitive *fs;
      // for (int i = 0; i < ptaVector->size(); i++) {
      //   ptaVector->at(i)->dumpCPts();
      //   if ( fs = dyn_cast<FlowSensitive>( ptaVector->at(i) ) ){
      //     ptd = fs->getPtd();
      //   }
      // }



      // if ( BVDataPTAImpl::DFPTDataTy *dt =dyn_cast<BVDataPTAImpl::DFPTDataTy>( ptd ) ){
      //   errs()<<"NICE \n";
      //   //Suite dans PointsToDFDS
      // }
      //
      // else if (incDataTy =  dyn_cast<BVDataPTAImpl::IncDFPTDataTy>( ptd ) ){
      //   errs()<<"NICE INC\n";
      //   //Suite dans PointsToDFDS
      // }

      errs() << "OK";


      errs() << "AndersenItf: ";
      errs().write_escaped(F.getName()) << '\n\n';
      Value *premOperand, *secOperand, *operand;
      std::string nameI,nameV,type,s,c, nameT,name1,name2,blockName, aloc, istLabel;

      Function::iterator be;
      unsigned int opcode, opnt_cnt, cpt;
      MemoryLocation precMemory;
      //Notre vecteur qui contiendra notre PointsToSet
      std::vector<const Value*> *ptsSet = new std::vector<const Value*>();
      std::string precName = "";
      int i = 0;



      FlowSensitive* fs = FlowSensitive::createFSWPA(svfModule);

      fs->getPAG()->print();

      for (Function::iterator b = F.begin(),  be = F.end(); b != be; ++b) {
        for (BasicBlock::iterator inst = b->begin(), e1 = b->end(); inst != e1; ++inst){
          opcode=inst->getOpcode(); istLabel=inst->getOpcodeName(opcode); opnt_cnt=inst->getNumOperands();
          if (LoadInst *LI = dyn_cast<LoadInst>(inst)) {
            for(cpt=0; cpt<opnt_cnt;cpt++){
      				operand=inst->getOperand(cpt);
              nameV=operand->getName();
              PointsTo pts = fs->getPts(fs->getPAG()->getValueNode(operand));
              errs()<<"\n\n\n";
              errs()<<"POINTS_TO_LOAD: "<<"\n";
              for (PointsTo::iterator it = pts.begin(), eit = pts.end(); it!=eit; ++it){
                errs()<<*it<<" ";
              }
              errs()<<"\n";

              secOperand = operand;
            }
            i++;
          }
          else if (StoreInst *SI = dyn_cast<StoreInst>(inst)) {
            operand = inst->getOperand(1);
            Type *T = operand->getType();
            i++;
            PointsTo pts = fs->getPts(fs->getPAG()->getValueNode(operand));
            //fs->getPAG()->getLocationSetFromBaseNode(fs->getPAG()->getValueNode(operand)).dump();
            errs()<<"\n\n\n";
            errs()<<"POINTS_TO_STORE: "<<"\n";
            for (PointsTo::iterator it = pts.begin(), eit = pts.end(); it!=eit; ++it){
              errs()<<fs->getPAG()->getPAGNode(*it)->getValueName()<<" ";
            }
            errs()<<"\n";

          }
          else if(CallInst* CA = dyn_cast<CallInst>(inst)){
            errs()<<"CAAAAL:::"<<"\n";
          }
          else if(GetElementPtrInst* GEP = dyn_cast<GetElementPtrInst>(inst)){
          }
          else if(CallInst* CI = dyn_cast<CallInst>(inst)){

            //if(A->getPointsToSet(CI->getOperand(0),*ptsSet)){
              //errs()<<"POINTS_TO_SET_CI FOR "<<CI->getOperand(0)<<"\n";
              ////printPointToSet(ptsSet);
            //}
          }
        }
      }
      // A->dumpConstraints();
      // std::vector<const llvm::Value*> allocSites;
      // A->getAllAllocationSites(allocSites);
      // errs()<<"ALLOC_SITE:"<<"\n\n";
      // std::sort(allocSites.begin(), allocSites.end());
      // for (size_t i = 0; i < allocSites.size(); i++) {
      //   errs()<<i<<" ->  "<<allocSites.at(i)<<"\n";
      // }
      //   for(Instruction &I: B){
      //     if(StoreInst* store_inst = dyn_cast<StoreInst>(&I)){
      //       for (auto op = I.op_begin(); op != I.op_end(); op++){
      //         errs() << I.getOperand(0) << I
      //         premOperand=inst->getOperand(0); secOperand=inst->getOperand(1); nameV=secOperand->getName(); idVar2=variables[nameV];
      //         op+=1;
      //         if(op!=I.op_end()){
      //           Value* v2 = op->get();
      //           AliasResult ar = AA->alias(v1,v2);
      //         }
      //     }else if(LoadInst* load_inst = dyn_cast<LoadInst>(&I)){
      //
      //     }
      //   }
      // }
      errs()<<"\n\n\n\n\n";
    }



    void getAnalysisUsage(AnalysisUsage &AU) const override {
      AU.addRequired<WPAPass>();
    }
  };
}

char AndersenItf::ID = 0;
static RegisterPass<AndersenItf> X("AndersenItf", "AndersenItf Pass");
