
% -------------------------------------------------------------------------------
# To install LLVM 

mkdir LLVM
cd LLVM/
svn co http://llvm.org/svn/llvm-project/llvm/trunk llvm
cd llvm/tools/
svn co http://llvm.org/svn/llvm-project/cfe/trunk clang
cd ..
mkdir build
cd build/
/local/mounier/CMAKE/cmake-3.7.2-Linux-x86_64/bin/cmake -DCMAKE_INSTALL_PREFIX=. ..
make # ou make -j 8, mais attention aux pbs de memoire virtuelle !

% -------------------------------------------------------------------------------

# To add a new LLVM Pass in the directory /lib/Transforms 

(see the tutorial: http://llvm.org/docs/WritingAnLLVMPass.html)

- create a new directory in llvm/lib/Transforms (not in build !)
- In this directory: 
	- add the source files (.cpp) of the new pass
	- add an (empty/not empty) "export" file
   this file is empty if your pass do not export anything for a following pass
- modify the file "CMakeLists.txt" of directory Transform ot add your pass 

# compile your pass by running  "make" (in directory build) 
   you should of course compile it again whenever you change the source code

% -------------------------------------------------------------------------------

# To run the pass

LLVM_BIN= your directory LLVM/llvm/build/bin 
BUILD=  your directory LLVM/llvm/build/


- Create an example file ".bc"
    $LLVM_BIN/clang -c -emit-llvm ex1.c

- Run the pass

    $LLVM_BIN/opt -load $BUILD/lib/LLVMflowSens2.so -fSens2 < ex1.bc



% -------------------------------------------------------------------------------

Useful links
============

http://llvm.org/docs/WritingAnLLVMPass.html

https://www.cs.cornell.edu/~asampson/blog/llvm.html

laure.gonnord.org/pro/research/ER03_2015/lab3_intro.pdf

