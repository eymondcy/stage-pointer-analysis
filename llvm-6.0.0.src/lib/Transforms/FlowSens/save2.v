


#include<map>
#include <stdlib.h>
#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include <llvm/IR/LLVMContext.h>
#include "llvm/ADT/PostOrderIterator.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/SCCIterator.h"
#include "llvm/IR/CFG.h"
#include "llvm/Analysis/AndersenAA.h"
#include "llvm/Analysis/Andersen.h"
// #include "llvm/Analysis/Dominators.h"
#include "llvm/Analysis/CallGraph.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/AssemblyAnnotationWriter.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/SetOperations.h"
#include "llvm/ADT/GraphTraits.h"
#include "llvm/IR/PassManager.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <unordered_set>
#include <deque>
#include <algorithm>
const int nbBM = 1000;
const int  nbvM = 1000;

using namespace llvm;

/* conversion case 2 coord. to one coord. */

int case2T1(int i, int j, int dim){
  return i*dim+j;
}

/* conversion case 3 coord. to one coord. */

int case3T1(int x, int y, int z, int dim){
  return x*dim*dim+y*dim+z;
}

void printAddressSpace(std::map<int, const llvm::Value*> idVarToAdress, std::map<int, std::string> inVariables){
  for(auto& p:idVarToAdress){
    errs()<<"LA VARIABLE "<<inVariables[p.first]<<"POINTE SUR:\n";
    errs()<<"\t -> "<<idVarToAdress[p.first]<<"\n";
  }
}

void print2Dp(int *t, int size)
{
  int i,j;
  errs() <<"\n\t";
  for (i=0;i<size;i++)
    errs() <<i<<"\t";//    printf("%i\t",i);
   errs() <<"\n\n";
  for (i=0;i<size;i++)
    {
   errs() <<i<<"\t";//        printf("%i\t",i);
    for (j=0;j<size;j++)
      errs() <<t[case2T1(i,j,size)]<<"\t";
    errs() <<"\n\n";
    /*printf("%i\t",t[case2T1(i,j,size)]);
      printf("\n\n");*/
    }
}

void print2DMp(int *t, std::map<int, std::string> m, int size)
{
  int i,j;
  errs() <<"\n\t";
  for (i=0;i<size;i++)
    errs() <<m[i]<<"\t";
  errs() <<"\n\n";
  for (i=0;i<size;i++)
    {
      errs() <<m[i]<<"\t";
    for (j=0;j<size;j++)
      errs() <<t[case2T1(i,j,size)]<<"\t";
    errs() <<"\n\n";
    }
}

void print3Dp(int *t, int id, std::map<int, std::string> m, int size)
{
  int i,j;
  errs() <<"\n\t";
  for (i=0;i<size;i++)
    errs() <<m[i]<<"\t";
  errs() <<"\n\n";
  for (i=0;i<size;i++)
    {
      errs() <<m[i]<<"\t";
    for (j=0;j<size;j++)
      errs() <<t[case3T1(id,i,j,size)]<<"\t";
    errs() <<"\n\n";
    }
}

   /*initialize all the elements of a table to zero*/
     void initialisationAZero(int*tab, int dim1, int dim2){
	int i, j;
	for(i=0; i<dim1;i++)
		for(j=0;j<dim2;j++){
			tab[i*dim1+j]=0;
		}

     }
   /*initialize the diagonal elements of a square matrix to 1 and the rest to zero*/
     void initialisationEntry(int* tab , int nbvV){
	int i, j;
		for (i=0;i<nbvV;i++)
		for (j=0;j<nbvV;j++){
			tab[i*nbvV+j]=0;
			if((i==j) && i!=0)
				tab[i*nbvV+j]=1;

		}
     }
      /*initialize all the elements of a square matrix to zero*/
     void initialisation(int* tab , int nbvV){
	int i, j;
	for (i=0;i<nbvV;i++)
		for (j=0;j<nbvV;j++){
			tab[i*nbvV+j]=0;
		}
     }
  /*initialize the diagonal elements of a par. matrix to 1 and the rest to zero*/
void initialisationEntry3D(int* tab , int nbvV, int nbbB){
       int i, j, b;
		for (b=0;b<nbbB;b++)
		for (i=0;i<nbvV;i++)
		for (j=0;j<nbvV;j++){
			tab[b*nbvV*nbvV+i*nbvV+j]=0;
			if((b==1) && (i==j) && i!=0)
				tab[b*nbvV*nbvV+i*nbvV+j]=1;

		}
     }
      /*initialize all the elements of a par. matrix to zero*/
     void initialisation3D(int* tab , int nbvV, int nbbB){
       int i, j,b;
		for (b=0;b<nbbB;b++)
		for (i=0;i<nbvV;i++)
		for (j=0;j<nbvV;j++){
			tab[b*nbvV*nbvV+i*nbvV+j]=0;
		}
     }

/* to = from[id] */
     void copy3D(int* from , int id, int* to, int nbvV){//move data from 3D table 'from' to an 2D table 'to'
	int i, j;
	for(i=0;i<nbvV;i++)
		for(j=0;j<nbvV;j++)
		  to[case2T1(i,j,nbvV)]=from[case3T1(id,i,j,nbvV)];
     }

      /* returns 1 if B1 is a dominator of B2 */
     bool B1DomB2(int B1, int B2,int n, int *tab){
	if (tab[B1*n+B2]==1)
		return 1 ;
        return 0;
     }

    /* returns 1 if B1 is a post dominator of B2 */
     bool B1PDomB2(int B1, int B2,int n, int *tab){
	if (tab[B1*n+B2]==1)
		return 1 ;
        return 0;
     }

      bool commonPDom(int B1, int B2, int n, int *dom, int *pDom){//pour vérifier la condition iii de la définition de l'influence region
	int i;
	for(i=1;i<n;i++)
		if(i!= B1 && i!= B2 && B1DomB2(i, B2, n, dom))
			if(B1PDomB2(i,B1, n, pDom))
				return 1;
	return 0;
     }

/* res = a + b*/
 int newAdd(int a, int b){
  	if(a==b)
		return a;
	if ((a==0) || (b==0))
	  return (a+b);
	if( (a==1 && b==2) || (a==2 && b==1)||(a==3) || (b==3) )
		return 3;

	return -1;
	// LM - What should be returned if a or b not in {0,1,2} ???
     }

   /*   g[b](id1)=g[b](id1)+g[b](id2)   */

     void newAddAll3D(int* gam, int b, int nbvV, int id1, int id2){//newadd pour toutes les cases si la var a modifier n'est de sortie
	int j;
	errs() <<"\ng["<<b<<"]("<<id1<<")=g["<<b<<"]("<<id1<<")+g["<<b<<"]("<<id2<<")\n";
	for (j=1;j<nbvV;j++){
	  gam[case3T1(b,id1,j,nbvV)]= newAdd(gam[case3T1(b,id1,j,nbvV)],gam[case3T1(b,id2,j,nbvV)]);
	}
     }

 /*tab1[i1] = tab1[i1] + tab2[i2]   */

     void newAddAllPred3D(int* tab1,int i1, int* tab2, int i2, int nbvV){
       //       int* aux= (int*)malloc(nbvV*nbvV* sizeof(int) );
       int i,j;
      	errs() <<"\ng["<<i1<<"]=g["<<i1<<"]+g["<<i2<<"]\n";
	for (i=0;i<nbvV;i++)
		for (j=0;j<nbvV;j++)
		  tab1[case3T1(i1,i,j,nbvV)]= newAdd(tab1[case3T1(i1,i,j,nbvV)],tab2[case3T1(i2,i,j,nbvV)]);
	//get(aux,tab1,nbvV);
     }

 /*tab1[i1] = tab1[i1] + tab2   */

     void newAddAllPred3D2D(int* tab1,int i1, int* tab2, int nbvV){
       //       int* aux= (int*)malloc(nbvV*nbvV* sizeof(int) );
       int i,j;
      	errs() <<"\ng["<<i1<<"]=g["<<i1<<"]+gOld"<<"\n";
	for (i=0;i<nbvV;i++)
		for (j=0;j<nbvV;j++)
		  tab1[case3T1(i1,i,j,nbvV)]= newAdd(tab1[case3T1(i1,i,j,nbvV)],tab2[case2T1(i,j,nbvV)]);
	//get(aux,tab1,nbvV);
     }


     bool compare(int* tab1, int* tab2, int nbvV){
	int i, j;
	for(i=0;i<nbvV;i++){
		for(j=0;j<nbvV;j++){
			if(tab1[i*nbvV+j]!=tab2[i*nbvV+j])
				return 0;
		}
	}
	return 1;
     }
/* if tab[id] == tab2 then 1 else 0 */
     int equal3D(int* tab1, int id, int* tab2,  int nbvV){
	int i, j;
	for(i=0;i<nbvV;i++){
		for(j=0;j<nbvV;j++){
			if(tab1[case3T1(id,i,j,nbvV)]!=tab2[case2T1(i,j,nbvV)])
			  {return 0;}
		}
	}
	return 1;
     }

   /*executes the necessary operations when id is an id of an output variable :  g[b] = g[b] |> id*/

     void replOutDep(int* gam, int b,int nbvV, int id){
	int i, j;
	//int* aux= (int*)malloc(nbv*nbv* sizeof(int) );
	errs() <<"\ng["<<b<<"]=g["<<b<<"]|>"<<id<<"\n";
	for(i=1;i<nbvV;i++){
	  if (i!=id){
	    if((gam[case3T1(b,i,id,nbvV)]==2||gam[case3T1(b,i,id,nbvV)]==3)){
			for(j=1;j<nbvV;j++){
				gam[case3T1(b,i,j,nbvV)]=newAdd(gam[case3T1(b,i,j,nbvV)] , gam[case3T1(b,id,j,nbvV)]);
			}
			gam[case3T1(b,i,id,nbvV)]=gam[case3T1(b,i,id,nbvV)]-2;
		}
	  }
	}
     }

/* tab1[i1] = tab1[i1] |> j for all  j such that exists i such that  tab1[i1][i][j]>=2>tab2[i][j]>0 followed by tab1[i1]= tab1[i1]+tab2 */

     void join3D2D(int* tab1,int i1, int* tab2, int nbvV){
       int i,j,change=1;
      	errs() <<"\ng["<<i1<<"]=g["<<i1<<"]|>*"<<"\n";
	while(change){
	  change =0;
	  for (i=0;i<nbvV;i++)
		for (j=0;j<nbvV;j++)
		  {
		    if  ((tab1[case3T1(i1,i,j,nbvV)]>=2) && (tab2[case2T1(i,j,nbvV)]<2))// && (tab2[case2T1(i,j,nbvV)]>0))
		      {
			change=1;
			replOutDep(tab1,i1,nbvV,j);
		      }
		  }
	}
	newAddAllPred3D2D(tab1, i1, tab2, nbvV);


     }

    /*executes  "store v1 *v2", i.e. id2=id1, where id1 is the id of v1 and id2 is that of v2*/
/*   g[b](id1)=0   */
void assignCst(int* gam, int b, int nbvV, int id1){
	int j;
	errs() <<"\ng["<<b<<"]("<<id1<<")=0\n";
	for (j=1;j<nbvV;j++){
		gam[case3T1(b,id1,j,nbvV)]= 0;
	}
}


//Dans le cas de l'appel à point to set, les dépendances seront liés à une variable
//Temporaire LLVM généralement ici appelé LOADX (avec X un numéro)
//On ne peut pas se permettre d'utiliser l'assign normal étant donné que ce dernier
//Ecrase la totalité de la ligne sans tenir compte des valeurs précédentes
void assignPtSet(int* gam, int b, int nbvV, int id1, int id2){
	int j;
	errs() <<"\ng["<<b<<"]("<<id1<<")=g["<<b<<"]("<<id2<<")\n";
	for (j=1;j<nbvV;j++){
    if(gam[case3T1(b,id1,j,nbvV)]!=0)
		gam[case3T1(b,id1,j,nbvV)]= gam[case3T1(b,id2,j,nbvV)];
	}
}



/*   g[b](id1)=g[b](id2)   */

void assign(int* gam, int b, int nbvV, int id1, int id2){
	int j;
	errs() <<"\ng["<<b<<"]("<<id1<<")=g["<<b<<"]("<<id2<<")\n";
	for (j=1;j<nbvV;j++){
		gam[case3T1(b,id1,j,nbvV)]= gam[case3T1(b,id2,j,nbvV)];
	}
}

   /*   g(id1)=id2_bar   */

 void assignOut(int*gam, int b, int nbvV, int id1, int id2){//on l'utilise s'il faut distinguer entre le var1 s'il est dans out ou non
	int j;
	errs() <<"\ng["<<b<<"]("<<id1<<")="<<id2<<"-bar\n";
	for (j=1;j<nbvV;j++)
		gam[case3T1(b,id1,j,nbvV)]= 0;
 	gam[case3T1(b,id1,id2,nbvV)]=2;//sinon on marque seulement que var2 depend de id1
     }



/* dest = src */
    void copie(int* src, int* dest, int nbvV){
	int i, j;
	for(i=0;i<nbvV;i++){
		for(j=0;j<nbvV;j++){
		   dest[i*nbvV+j]=src[i*nbvV+j];
		}
	}
     }

   void transp(int* src, int* dest, int nbvV){
	int i, j;
	for(i=0;i<nbvV;i++){
		for(j=0;j<nbvV;j++){
		   dest[j*nbvV+i]=src[i*nbvV+j];
		}
	}
     }
    /* Chris : r3 = r2 o r1 */
    int plus1(int a, int b){
      if (a+b>=1){return 1;}else{return 0;}
    }

    int prod1(int a, int b){
      return a*b;
    }

    void compR(int* r1, int* r2, int* r3,  int nbvV){
      int i, j,k;

	for(i=0;i<nbvV;i++){
		for(j=0;j<nbvV;j++){

		  r3[i*nbvV+j]=0;

		  for(k=0;k<nbvV;k++){
		    r3[i*nbvV+j]=plus1(r3[i*nbvV+j], prod1(r1[i*nbvV+k],r2[k*nbvV+j] ));
		    }

	}
     }
    }

    void unionR(int* r1, int* r2, int* r3,  int nbvV){
      int i, j;

	for(i=0;i<nbvV;i++){
		for(j=0;j<nbvV;j++){
		    r3[i*nbvV+j]=plus1(r1[i*nbvV+j] ,r2[i*nbvV+j]);
		    }

	}
     }


    /* rp cloture reflexive de r */
     void closureR(int* r, int* rp, int nbbB){ // pour calculer les prede et les succ qui sont pas immédiats
        int i;
        copie(r,rp,nbbB);
	for(i=0;i<nbbB;i++){
	  rp[i*nbbB+i]=1;
	}
     }

   /* rp cloture transitive de r */
      void closureT(int* r, int* rp, int nbbB){ // pour calculer les prede et les succ qui sont pas immédiats
       int *aux=(int*)malloc(nbbB*nbbB* sizeof(int) );
	copie(r,rp,nbbB);
	compR(r,r,aux,nbbB);
	unionR(r,aux,aux,nbbB);
	while (!(compare(rp,aux,nbbB)))
	  {
	    copie(aux,rp,nbbB);
	    compR(rp,r,aux,nbbB);
	    unionR(r,aux,aux,nbbB);
	    }
	free(aux);
     }


    /* chemin a ->  n sans passer par x */

    int path(int* r, int nbvV, int a, int n,  int x){
      int i, ans;
      int *aux=(int*)malloc(nbvV*nbvV* sizeof(int) );
      int *auxB=(int*)malloc(nbvV*nbvV* sizeof(int) );
      copie(r,aux,nbvV);
      for(i=0;i<nbvV;i++){
	     aux[x*nbvV+i]=0;
	     aux[i*nbvV+x]=0;
	}
      closureT(aux,auxB,nbvV);
      closureR(auxB,auxB,nbvV);
      ans = auxB[a*nbvV+n];
      free(aux);
      free(auxB);
      return ans;
    }


    void nonImmediate(int* r, int* rp, int nbbB){
	   return closureT(r,rp,nbbB);
	 }


void mapToMatrixBlock(std::map<int,std::vector<BasicBlock*>> mapBlock, std::map<BasicBlock*,int> idBlocks, int* tab, int nbbB){
	std::vector<BasicBlock*> vecTemp;
 	unsigned int idTemp1, j;
	for(std::map<int,std::vector<BasicBlock*>>::iterator m=mapBlock.begin(); m!=mapBlock.end(); ++m) {// transformer le map succ en une matrice, suc[i][j] donc j est un suc de i afin de faciliter la manipulation des sucs

		vecTemp=m->second;
		//printf("Size = %i\n",  vecTemp.size());
		for(j=0;j<vecTemp.size();j++){
		  //errs() <<" "<<j;
			//blockName=vecTemp[j]->getName();
			idTemp1=idBlocks[vecTemp[j]];
			tab[(m->first)*nbbB+idTemp1]=1;//idTemp est un succ de i
		}
 	}
     }

     void mapToMatrixInt(std::map<int, std::vector<int>> mapInt, int* tab, int nbbB){
 	unsigned int idBlock, i;
	std::vector<int> vecTemp;
	for(std::map<int, std::vector<int>>::iterator it=mapInt.begin(); it!=mapInt.end(); ++it) {
		idBlock=it->first;
		vecTemp=it->second;
		for(i=0;i<vecTemp.size();i++){
			tab[idBlock*nbbB+vecTemp[i]]=1;
		}
   	}
     }

    /*determines the immediate dominator for each block*/
/*  void domImm(int* dom, int nbbB, std::map<int,int> &domIm){
      int i, j, change;
	for (i=1;i<nbbB;i++){
	  domIm[i]=i;
	}
	change=1;
	while (change){
	  change = 0;
	  for (i=1;i<nbbB;i++){
	    for (j=1;j<nbbB;j++){
	      if((j!=i) && (dom[case2T1(j,i,nbbB)]==1) &&  ((domIm[i]==i) || dom[case2T1(domIm[i],j,nbbB)==1]))
		{// si j domine i et  domIm[i] domine j
		  domIm[i]=j; change=1;
		}
	    }
	  }
	}
   }
*/
   /*determines the immediate dominator for each block*/
    void domImm(int* dom, int nbbB, std::map<int,int> &domIm){
      int i, j, change;
	for (i=1;i<nbbB;i++){
	  domIm[i]=i;
	}
	change=1;
	while (change){
	  change = 0;
	  for (i=1;i<nbbB;i++){
	    for (j=1;j<nbbB;j++){
	      //errs() <<"\n dom[case2T1(domIm["<<i<<"],"<<j<<","<<nbbB<<")]="<<dom[case2T1(domIm[i],j,nbbB)]<<"\n";
	      //errs() <<"\n dom[case2T1("<<j<<","<<i<<","<<nbbB<<")]="<<dom[case2T1(j,i,nbbB)]<<"\n";
	      //errs() <<"\n*****";
	      if((j!=i) && (dom[case2T1(j,i,nbbB)]==1) && (domIm[i]!=j) && ((domIm[i]==i) || (dom[case2T1(domIm[i],j,nbbB)]==1)))
		{// si j domine i et  domIm[i] domine j
		  //errs() <<"\n domIm["<<i<<"]="<<j<<"\n";
		  domIm[i]=j; change=1;
		}
	    }
	  }
	}
   }


       /*determines the dependencies for each block*/
void depend(int *dom, int *pDom, int n, int *nbSuc, std::map<BasicBlock*,int> idBlocks, std::vector<BasicBlock*> blocks,  std::map<int , std::vector<BasicBlock*>> &dependences){//calculate the dependencies of each block , the implementation is similar to the function all Influence regions
	unsigned int i, j, id1, id2;
	std::string blockName;
	for(i=0; i<blocks.size(); i++){
		std::vector<BasicBlock*> vecTemp;
		id1=idBlocks[blocks[i]];
		for(j=0;j<blocks.size(); j++){
			id2=idBlocks[blocks[j]];
			if((dom[id2*n+id1]==1)&& (!(pDom[id1*n+id2]==1)) && (id1!=id2) && nbSuc[id2]>1) //id1 depends on id2
				vecTemp.push_back(blocks[j]);
		}
		dependences[id1]=vecTemp;
	}
     }

void reg(int* suc, int* pre, unsigned int nbbB, std::map<int,int> domIm, std::map<int,std::vector<int>> &regionMap){// determine la region entre tous les blocks et leurs dominateurs immediats
	unsigned int i, idBlock, idDomIm;
	std::vector<int> vecTemp;
	for(std::map<int, int>::iterator it=domIm.begin(); it!=domIm.end(); ++it) {
		idBlock=it->first;
		idDomIm=it->second;
		if(idBlock!=idDomIm){
			for(i=1; i<nbbB; i++){
			  if (path(suc, nbbB, idDomIm, i, idBlock) && path(pre, nbbB, idBlock, i, idDomIm))// && i!=idDomIm)
			    	vecTemp.push_back(i);
			}
		}
		vecTemp.push_back(idDomIm);
		regionMap[idBlock]=vecTemp;
		vecTemp.erase (vecTemp.begin(),vecTemp.end());
        }
     }

     /*if we have more than one path that leads to idBlock1 from its immediate dominator, and if idblock2 is a pred of idblock1, this function returns a vector that contains the ids of the blocks that are in the reg between the idblock1 and its immediate pred, but are not antecedent of idblock2 */

     std::vector<int> intersectionPredReg(int* suc, int idBlock1, int idBlock2, unsigned int nbbB, std::map<int,std::vector<int>> regionMap){// idBlock2 est un pred de idBlock1
	unsigned int i;
	std::vector<int> vecTemp, vecTempMap;
        vecTempMap=regionMap[idBlock1];
	for(i=1;i<nbbB;i++){
		if ( std::find(vecTempMap.begin(), vecTempMap.end(), i) != vecTempMap.end() )// i se trouve dans la region de idBlock1
		  if(path(suc, nbbB, i, idBlock1, idBlock2)==1)// mais i n'est pas un pere de idBlock2
				vecTemp.push_back(i);
	}
	for(i=0;i<vecTemp.size();i++){
	  //errs()<<"idBlock en bas"<<idBlock1<<"\n";
		errs()<<vecTemp[i]<<" between immediate dom and "<<idBlock1<<" but not before "<<idBlock2<<"\n";
	  //errs()<<vecTemp[i]<<"\n";
	}
	return vecTemp;
     }



   /*returns the id of the conditional branch variable presents in a block*/

int brDependence(BasicBlock *bb, std::map<std::string, int> variables){// va retourner la valeur de l'id de la variables utilisee pour le br dans un depend d'un block
	int idV;
	unsigned int opCode;
	Instruction* inst;
	std::string instName, blockName,nameV, instLabel;
	Value *premOperand;
	//bb=inIdBlocks[indice];
	//	errs()<<"dep des block: block "<<bb->getName()<<"\t";
	inst=bb->getTerminator();
	// errs()<<"terminator"<<inst<<"\n";
	opCode=inst->getOpcode();
	instLabel=inst->getOpcodeName(opCode);
	if(instLabel=="br"){
		premOperand=inst->getOperand(0);
		nameV=premOperand->getName();
		idV=variables[nameV];
		return idV;
		//errs() <<"\n branch variable : "<<idV;;
	}
	return 0;
     }

 /*call newAddAllBr3D for all the dependencies of a block*/

void CalculDepBr3D(std::map<int , std::vector<BasicBlock*>> dep, std::map<BasicBlock*,int> idBlocks, std::map<std::string, int> variables, int *gam, unsigned int idBlock, int idVar,int nbvV){
	BasicBlock *bb;
	std::map<int, std::vector<BasicBlock*>>::iterator it;
	std::vector<BasicBlock*> vecTemp;
	unsigned int i, idVarBr, idTemp1;
	//	errs() <<"\ncall  CalculDepBr3D block : "<<idBlock<<" Var : "<<idVar<<"\n";;
     	it=dep.find(idBlock);
	if(it!=dep.end()){
		vecTemp=it->second;
		for(i=0;i<vecTemp.size();i++){
			bb=vecTemp[i];
		    	idTemp1=idBlocks[bb];
			//errs() <<"\n idB : "<<idTemp1;;
			if (idTemp1!=idBlock){
			  idVarBr=brDependence(bb, variables);
			  // errs() <<"\n idVarBr  : "<<idVarBr;;
			if (idVarBr!=0)
			  newAddAll3D(gam, idBlock, nbvV, idVar, idVarBr);
			}
		}
      	}
     }

namespace {
   class workList{
   public:
     //std::unordered_set<BasicBlock*> inList;
     //std::deque<BasicBlock*> work;
                std::unordered_set<int> inList;
		std::deque<int> work;
     //public:
     //template<typename IterTy> workList(IterTy i, IterTy e) : inList(i,e), work(i,e){}
              workList(){}

     //BasicBlock*
	int take(){
		int front =work.front();
		work.pop_front();
		inList.erase(front);
		return front;
	}

	bool empty(){
		return work.empty();
	}

	void add(int bi){
	  	if(inList.count(bi)==0){
			work.push_back(bi);
		}
		inList.insert(bi);
	}
     };

  struct flowSens2: public FunctionPass {
     AndersenAAResult *AA;
     std::vector<const llvm::Value*> allocSites;

     Andersen *A;
     static char ID;
     bool b;
     std::map<std::string, int> variables;
     std::map<int, std::string>inVariables;

     std::map<int, const llvm::Value*> idVarToAdress;
     std::map<const llvm::Value*, int> adressToIdVar;
     std::vector<const llvm::Value*> AM;

     int counterInstForAdresses;

     std::vector<BasicBlock*> blocks;
     std::map<BasicBlock*,int> idBlocks;
     std::map<int,BasicBlock*> inIdBlocks;
    //std::map<int , std::vector<BasicBlock*>> IR;//influence regions for all blocks
     std::map<int, std::vector<BasicBlock*>> succ;
     std::map<int, std::vector<BasicBlock*>> pred;
     std::map<int , std::vector<BasicBlock*>> dependences;//dependences of each block, note that the vec is of bb and not string, cz I need to get the terminator of each block that ablock depends on
     std::map<int,std::string> types;
     std::map<int,int> domIm;
     std::map<int,std::vector<int>> regionMap;
     std::map<int, std::vector<int>> outputsPerBlock;
     std::vector <std::vector<std::string>> depVar;
     std::vector<unsigned int> outputs;

     flowSens2() : FunctionPass(ID){}


    virtual void getAnalysisUsage(AnalysisUsage &AU) const override {
      AU.addRequired<DominatorTreeWrapperPass>();
      AU.addRequired<PostDominatorTreeWrapperPass>();

      //Andersen PASS
      AU.addRequired<AndersenAAWrapperPass>();
     }

     virtual bool runOnFunction(Function &F){
       std::string currentAddressCall="";


       counterInstForAdresses = 1;
       AA = &getAnalysis<AndersenAAWrapperPass>().getResult();
       A = AA->getAndersenResult();
       A->getAllAllocationSites(allocSites);
       std::sort(allocSites.begin(), allocSites.end());
       std::string nameI,nameV,type,s,c, nameT,name1,name2,blockName, aloc, istLabel;
       Constant *CI;
       std::map<std::string, int>::iterator itt;
       unsigned int opcode,id=1, idTemp,cpt=0,idBlock=1,nbbB,nbvV;/*,nbv=0*/
       Value *premOperand, *secOperand, *operand;
       unsigned int opnt_cnt;
       errs ()<<"function: "<<F.getName()<<"\n";
       ReversePostOrderTraversal<Function*> rpot(&F);
       //workList work(std::begin(rpot), std::end(rpot));
       int *nbSuc=new int[(nbBM+1)];
       workList work;
       for (User *U : F.users()) {
         if (Instruction *Inst = dyn_cast<Instruction>(U)) {
               errs() <<"Call instruction: "<< *Inst << "\n";
  	     }
       }
       /*unsigned int nbB=0;*/
       nbbB=0;
       for(Function::iterator BB = F.begin(), E = F.end(); BB != E; ++BB) {
          nbbB++;
      }
       /*
       for (i=0;i<nbBM+1;i++)
           nbSuc[i]=0;
       */
     for(BasicBlock &bb :F){
        blocks.push_back(&bb);//notes1
	       if (bb.hasName()){
          blockName=bb.getName();
         }
	       else{
		      bb.setName("block");
		      blockName=bb.getName();
	       }
        idBlocks[&bb]=idBlock;
	      inIdBlocks[idBlock]=&bb;
	      errs()<<"label: "<<blockName<<"\n";
	// tInst=bb.getTerminator();//get the block terminator instruction to determine the succ of each block
	      std::vector<BasicBlock*> vecTemp, vecTemp1;
	// BasicBlock *sucTemp, *predTemp;
/*****************************************************Les successeurs du bloc courant**********************************************************************/
        //for (unsigned Ic = 0, NSucc = tInst->getNumSuccessors(); Ic < NSucc; ++Ic) {//succ of each block and store them in the map succ
		//sucTemp = tInst->getSuccessor(Ic);
	     for (BasicBlock *sucTemp : successors(&bb)) {
    		vecTemp.push_back(sucTemp);
    		if (sucTemp->hasName())
    			blockName=sucTemp->getName();
    		else {
    			sucTemp->setName("blockS");
    			blockName=sucTemp->getName();
    		  }
		    errs()<<"succ: "<<blockName<<"\n";
        }//end for succ
	     succ[idBlock]=vecTemp;
	     nbSuc[idBlock]=vecTemp.size();
	     vecTemp.erase (vecTemp.begin(),vecTemp.end());
/*****************************************************Les prédécesseurs du blco courant**********************************************************/
	for (BasicBlock *predTemp : predecessors(&bb)) {
 		vecTemp1.push_back(predTemp);
		if (predTemp->hasName())
			blockName=predTemp->getName();
		else {
			predTemp->setName("blockP");
			blockName=predTemp->getName();
		}
		errs()<<"pred: "<<blockName<<"\n";
        }//end for pred
	pred[idBlock]=vecTemp1;
	vecTemp1.erase (vecTemp1.begin(),vecTemp1.end());
	idBlock++;
	/********************************************************/
     }

   /*************************************************Récupération de toutes les variables******************************************************/
    for(BasicBlock &bb :F){
      idBlock=idBlocks[&bb];
      	errs () << "\t\t"<< "get variables block: "<<idBlock<<"\n";
       for (BasicBlock::iterator I = bb.begin(), e = bb.end();I != e; ++I) {//iterate over all the instructions of each block
	 errs() << "block :" <<bb.getName()<<"\t: " << *I << "\n";
       if (I->isBinaryOp()){
          opcode=I->getOpcode();
          nameI=I->getOpcodeName(opcode);
	  if(!(I->hasName())){
		I->setName(nameI);
	  }
		name1=I->getName();// since this is a binary instruction , its name is the name of the new variable that contains the result
		inVariables[id]=name1;

		variables[name1]=id;//contains variables is and name
		errs () << "\t\t"<< "name: "<<name1<<"\n";
		errs()<< "\t\t"<< "idBinOp: "<<id<<"\n";
		id++;
       }
       else{
          opcode=I->getOpcode();
          nameI=I->getOpcodeName(opcode);
	  if (nameI=="alloca")
    {//if it is an alloca instruction
	  	if(!(I->hasName()))
		  I->setName("alloca");
  		nameT=I->getName();
  		variables[nameT]=id;// store the new variable in invariables and variables
  		inVariables[id]=nameT;

      std::vector<const llvm::Value*> ads;
      idVarToAdress[id]=allocSites.at(counterInstForAdresses);
      adressToIdVar[allocSites.at(counterInstForAdresses)]=id;
      counterInstForAdresses++;

  		aloc=="t"+std::to_string(id);//give the new variable an arbitrary type
  		types[id]=aloc;// store the variable and its type in the map types
  		errs() << "\t\t"<< "name:"<<nameT<<"\n";
  		errs() << "\t\t"<< "idalloca:"<<id<< "\n";
  		id++;
		//errs()<< "\t\t"<< "id++"<< id<<"\n";
	 }
	 else if (nameI=="load"){//load case :D
		I->setName("load");
		nameT=I->getName();// like binary operations, load instructions names are the name of the new variables where the values are stored after the read from the memory
		std::vector <std::string> vec;
		variables[nameT]=id;// store thenew variable in the vector variables and invariables
		inVariables[id]=nameT;
		id++;
		operand=I->getOperand(0);
		if(isa<GlobalVariable>(operand)){
			name1=operand->getName();
			itt=variables.find(name1);
			if(itt==variables.end()){
				variables[name1]=id;
				inVariables[id]=name1;
				id++;
			}
			idTemp=variables[name1];
			if ( std::find(outputs.begin(), outputs.end(), idTemp)== outputs.end())
				outputs.push_back(idTemp);
			//errs () << "\t\t"<< "add assigned variable: "<<idTemp<<"\n";
			//outputsPerBlock[idBlock].push_back(idTemp);
		}
	 }
   else if(nameI=="call")
   {
     if(CallInst* CI = dyn_cast<CallInst>(I)){
       if(CI->getCalledFunction()->getName()=="malloc"){

         AM.push_back(allocSites.at(counterInstForAdresses));
         currentAddressCall="callDone";
       }
     }
   }
	 else if (nameI=="store"){//store case :D
		operand=I->getOperand(1);
		if(isa<GlobalVariable>(operand)){
			name1=operand->getName();
			itt=variables.find(name1);
			if(itt==variables.end()){
				variables[name1]=id;
				inVariables[id]=name1;
				id++;
			}
			idTemp=variables[name1];
			if ( std::find(outputs.begin(), outputs.end(), idTemp)== outputs.end())
				outputs.push_back(idTemp);
			        errs () << "\t\t"<< "add assigned variable: "<<idTemp<<"\n";
				outputsPerBlock[idBlock].push_back(idTemp);
		}
    if(currentAddressCall!=""){
      idVarToAdress[variables[operand->getName()]]=allocSites.at(counterInstForAdresses);
      adressToIdVar[allocSites.at(counterInstForAdresses)]=variables[operand->getName()];
      currentAddressCall="";
      counterInstForAdresses++;
    }
	 }
	 else if (nameI=="ret"){// return case ;)
		opnt_cnt=I->getNumOperands() ;
		cpt=0;
		while(cpt<opnt_cnt){
			operand=I->getOperand(cpt);
			if (!(CI = dyn_cast<Constant>(operand))){
				nameV=operand->getName();
				idTemp=variables[nameV];//get the id of the variable and store it in the vector of ouput variables outputs
				outputs.push_back(idTemp);

				errs() << outputs[cpt]<<"\n";
			}
			cpt++;
		}
	}
	else if (nameI=="icmp"){// icmp case , the same concept :)
		I->setName("icmp");
		nameT=I->getName();
		std::vector <std::string> vec;
		inVariables[id]=nameT;
		variables[nameT]=id;
		id++;
	}
      }// fin else
     }//fin for de l'instr
   }//fin for des blocks

   nbvV=inVariables.size();
   //int nbO=outputs.size();
   inVariables[0]="leak.";
   errs ()<< "variables number is " << nbvV<< "\n";


   for(std::map<std::string,int>::iterator itvv=variables.begin(); itvv!=variables.end(); ++itvv) {
     errs()<<"clef :\t"<< itvv->first<< "   :   ";
     errs()<<"valeur :\t"<< itvv->second<< "\n";
   }

   /*************************************************Déclarations et initialisations pour Kildall*******************************************************/

   printAddressSpace(idVarToAdress, inVariables);
   errs()<<"\n\nALLOC_SITE:"<<"\n\n";
   for (size_t i = 0; i < allocSites.size(); i++) {
     errs()<<i<<" ->  "<<allocSites.at(i)<<"\n";
   }
  unsigned int idVar1, idVar2, i=0,j=0,l=0, idTemp1,idTemp2;
  nbbB=blocks.size();
  int *gamma = new int[(nbvM+1)*(nbvM+1)];/*, alpha[nbv+1][nbv+1]*/;
  int *gammaTemp = new int[(nbvM+1)*(nbvM+1)];/*,alphaTemp[nbv+1][nbv+1]*/;
   int *outVar = new int[(nbvM+1)];;
   int *gammaPerBlock = new int[(nbBM+1)*(nbBM+1)*(nbBM+1)]; //= (int*)malloc((nbB+1)*(nbB+1)*(nbB+1) * sizeof(int) ); //new int[(nbB+1)*(nbB+1)*(nbB+1)]; /*, alphaPerBlock[nbB+1][nbv+1][nbv+1]*/;
   int *dominate=new int[(nbBM+1)*(nbBM+1)];//qui domine qui
   int *pDominate=new int[(nbBM+1)*(nbBM+1)];//qui postdomine qui
   int *blockDominatedBy=new int[(nbBM+1)*(nbBM+1)];//la liste de postdominateur de chaque block, utile pour influence region
   int *suc=new int[(nbBM+1)*(nbBM+1)];
   int *sucT=new int[(nbBM+1)*(nbBM+1)];
   int *visited=new int[(nbBM+1)];
   int *pre=new int[(nbBM+1)*(nbBM+1)];//, fils[nbB+1][nbB+1], peres[nbB+1][nbB+1];//suc[2][4]=1 alors 4 est un suc de 2
   //int outPerBlock[nbB+1][nbO+1];// on affécté la variable de sortie 3 dans le block 5 alors outputsPerBlock[5][3]=1
   int *outPerBlock=new int[(nbBM+1)*(nbBM+1)];// on affécté la variable de sortie 3 dans le block 5 alors outputsPerBlock[5][3]=1
   int *region=new int[(nbBM+1)*(nbBM+1)];// reg[2][1]=1 cad le block 1 se trouve dans la region du block 2

   BasicBlock* bb;//, *rim;
  std::vector<BasicBlock*> vecTemp;
  std::vector<int> vecInt;
  bool bl=0;
  DominatorTree& DT= getAnalysis<DominatorTreeWrapperPass>().getDomTree();
  SmallVector<BasicBlock*,10> domResult ;
  PostDominatorTree& PDT= getAnalysis<PostDominatorTreeWrapperPass>().getPostDomTree();
  SmallVector<BasicBlock*,10> pDomResult ;
  int et=0;

  int callDone=0;

  std::vector<const Value*> tempArrayForPtSet;

  initialisationAZero(dominate, nbbB+1, nbbB+1);
  initialisationAZero(pDominate, nbbB+1, nbbB+1);
  initialisationAZero(blockDominatedBy, nbbB+1, nbbB+1);
  initialisationAZero(suc, nbbB+1, nbbB+1);
  initialisationAZero(pre, nbbB+1, nbbB+1);
  initialisationAZero(outPerBlock, nbbB+1, nbbB+1);
  initialisationAZero(region, nbbB+1, nbbB+1);

  for (i=0;i<nbvV+1;i++)
         outVar[i]=0;

  for (i=0;i<nbbB+1;i++)
         visited[i]=0;

   errs()<<"number of outvar = "<<outputs.size() <<"\n";
  for (i=0;i<outputs.size();i++){
	j=outputs[i];
	outVar[j]=1;
        errs()<<"outvar  "<< j<<"\n";
  }

  initialisation(gamma, nbvV+1);//initial. de gamma
  //       errs()<<"Initial Gamma : gamma"<<"\n\n";
  //print2Dp(gamma, nbvV+1);
  initialisationEntry3D(gammaPerBlock, nbvV+1, nbbB+1);
  //    errs()<<"Initial Gamma : gammaPerBlock "<<"\n\n";
  //	print3Dp(gammaPerBlock, 1, inVariables, nbvV+1);
	errs ()<< "**************************************************************\n";
	errs ()<< "blocks.size : "<<blocks.size()<<"\n";
	for(i=0; i<blocks.size(); i++){
	  errs ()<<"block : "<<idBlocks[blocks[i]]<<"\n";

	}
	for(i=0; i<blocks.size(); i++){
	/*
	DT.getDescendants(blocks[i], domResult);
	idTemp1=idBlocks[blocks[i]];
	unsigned int dR= domResult.size();
	for(j=0;j<dR;j++){
		idTemp2=idBlocks[domResult[j]];
		dominate[case2T1(idTemp1,idTemp2,nbbB)]=1;
	}
	*/
	  idTemp1=idBlocks[blocks[i]];
	  for(j=0; j<blocks.size(); j++){
	    idTemp2=idBlocks[blocks[j]];
	    if (DT.dominates(blocks[i],blocks[j]))
		    {
		    dominate[case2T1(idTemp1,idTemp2,blocks.size()+1)]=1;
		    }
	  }
	}
	for(i=0; i<blocks.size(); i++){//dominators & postDominators
	  /*
	PDT.getDescendants(blocks[i], pDomResult);
	unsigned int dR= pDomResult.size();
	for(j=0;j<dR;j++){
		idTemp2=idBlocks[pDomResult[j]];
		pDominate[case2T1(idTemp1,idTemp2,nbbB+1)]=1;
		}
	  */
	   idTemp1=idBlocks[blocks[i]];
	  for(j=0; j<blocks.size(); j++){
	    idTemp2=idBlocks[blocks[j]];
	    if (PDT.dominates(blocks[i],blocks[j]))
		    {
		    pDominate[case2T1(idTemp1,idTemp2,blocks.size()+1)]=1;
		    }
	  }
        }
   nbbB=blocks.size();
   mapToMatrixBlock(succ, idBlocks, suc, nbbB+1);
   mapToMatrixBlock(pred, idBlocks, pre, nbbB+1);



   domImm(dominate, nbbB+1, domIm);

   reg(suc, pre, nbbB+1, domIm, regionMap);// calculer toutes les régions entre les block et ses dominateurs immédiats

   mapToMatrixInt(outputsPerBlock, outPerBlock, nbvV+1);//pour calculer les variable de sortie afféctées dans un block !!!!!!! error

   // errs()<<"Nb= "<<nbbB<<"\n pre"<<"\n\n";
   // print2Dp(pre, nbbB+1);

   mapToMatrixInt(regionMap, region, nbbB+1);


 errs()<<"nbbB= "<<nbbB<<"\n\n";
 errs()<<"blocks.size()= "<<blocks.size()<<"\n\n";
 depend(dominate, pDominate, nbbB+1,nbSuc,idBlocks, blocks, dependences);

  /************************************************************Kildall*********************************************************************/
   /*   while(!work.empty()){//cal. de pred.
     work.take();
   }
   */
   //bb=inIdBlocks[1];
  work.add(1);
  et=1;
  /* */
  closureT(suc, sucT, nbbB+1);
  /* */
  while(!work.empty()){//cal. de pred.
        et=et+1;
	idBlock=work.take();
	bb=inIdBlocks[idBlock];
	errs()<<"\n block :"<<idBlock<<" visited :"<<visited[idBlock]<<"\n";;
	//idBlock=idBlocks[bb];
	for(i=1;i<=nbbB;i++)
	  if(pre[case2T1(idBlock,i,nbbB+1)]==1){
	      vecInt=intersectionPredReg(suc, idBlock, i, nbbB+1, regionMap);
	      for(j=0;j<vecInt.size(); j++){// pour parcourir le vec
		 for(l=1;l<=nbvV;l++){// pour parcourir la matrice des variables
		    if(outPerBlock[case2T1(vecInt[j],l,nbvV+1)]==1 ){//les variables de sortie affectes dans les blocks qui sont dans reg mais pas dans pred
			   replOutDep(gammaPerBlock, i, nbvV+1, l);
		     }
		  }
	       }
	      for(j=1;j<=nbbB;j++){
		if((path(suc, nbbB+1, j, idBlock, i)==1) &&(path(suc, nbbB+1, idBlock, j,i)==1) && j!=idBlock){
		 for(l=1;l<=nbvV;l++){// pour parcourir la matrice des variables
		    if(outPerBlock[case2T1(j,l,nbvV+1)]==1 ){//les variables de sortie affectes dans les blocks qui sont dans reg mais pas dans pred
			   replOutDep(gammaPerBlock, i, nbvV+1, l);
		     }
		  }
		}
	      }


	      newAddAllPred3D(gammaPerBlock,idBlock, gammaPerBlock,i, nbvV+1);
	  }

	initialisation(gammaTemp, nbvV+1);

	copy3D(gammaPerBlock,idBlock, gammaTemp, nbvV+1);//to store the old gamma in order to compare it with the new one
	/*
	errs()<<"Etape : "<<et<<" Gamma"<<"["<<idBlock<<"] before change"<<"\n\n";
	print3Dp(gammaPerBlock, idBlock, inVariables, nbvV+1);
	errs ()<< "**************************************************************\n";
	*/
	for (BasicBlock::iterator inst = bb->begin(), e1 = bb->end();inst != e1; ++inst){//on itère sur les instructions de chaque B.
	  errs()<< idBlock<<" : "<<*inst<<"\n";
	  opcode=inst->getOpcode(); istLabel=inst->getOpcodeName(opcode); opnt_cnt=inst->getNumOperands();
		if(istLabel=="store"){//c tjrs 2ème opérande qui sera modifiée
      if(callDone==0){
        premOperand=inst->getOperand(0); secOperand=inst->getOperand(1); nameV=secOperand->getName(); idVar2=variables[nameV];
        Type *T = secOperand->getType();
        if(!(T->isPointerTy() && T->getContainedType(0)->isPointerTy())){//Si le second opérand n'est pas un pointeur
          if(outVar[idVar2]==1) {// si la deuxieme variable est une variable de sortie il faut:
            replOutDep(gammaPerBlock,idBlock, nbvV+1, idVar2);
            if(!(CI = dyn_cast<Constant>(premOperand))){//si var1 n'est pas une constante
              nameV=premOperand->getName();
              idVar1=variables[nameV];
              if(outVar[idVar1]!=1)
                assign(gammaPerBlock,idBlock, nbvV+1, idVar2, idVar1);
              else{
                assignOut(gammaPerBlock,idBlock, nbvV+1, idVar2, idVar1);
              }
            }
            else{//sinon on met les cases de var2 à zéro dans alpha
              for(j=1;j<=nbvV;j++){
                gammaPerBlock[case3T1(idBlock,idVar2,j,nbvV+1)]=0;
              }
            }
          }
          else{
            if(!(CI = dyn_cast<Constant>(premOperand))){
              nameV=premOperand->getName(); idVar1=variables[nameV];
              if(outVar[idVar1]!=1)
                {assign(gammaPerBlock,idBlock, nbvV+1, idVar2, idVar1);  }
              else
                { assignOut(gammaPerBlock,idBlock, nbvV+1, idVar2, idVar1); }
            }
            else{
              assignCst(gammaPerBlock,idBlock, nbvV+1, idVar2);
            }
          }
          CalculDepBr3D(dependences, idBlocks, variables, gammaPerBlock, idBlock, idVar2, nbvV+1);
        }
      }else{
        callDone=0;
      }
    }
		else if(inst->isBinaryOp()|| istLabel=="icmp" ){
			nameI=inst->getName(); idVar1=variables[nameI]; bl=0;//wich operand
			for(cpt=0; cpt<opnt_cnt;cpt++){
				operand=inst->getOperand(cpt);
				if(!(CI = dyn_cast<Constant>(operand))){
					nameV=operand->getName();
					idVar2=variables[nameV];

						if(bl==0){
							if(outVar[idVar2]!=1)
							  assign(gammaPerBlock,idBlock, nbvV+1, idVar1, idVar2);
							else
							  gammaPerBlock[case3T1(idBlock,idVar1,idVar2,nbvV+1)]=2;
							}
						else{
							if(outVar[idVar2]!=1)
							  newAddAll3D(gammaPerBlock,idBlock, nbvV+1, idVar1, idVar2);
							else
							  gammaPerBlock[case3T1(idBlock,idVar1,idVar2,nbvV+1)]=gammaPerBlock[case3T1(idBlock,idVar1,idVar2,nbvV+1)]+2;
						}
					//}//fin else
					bl++;
				}//fin if cast

			}//fin for opnt
		CalculDepBr3D(dependences, idBlocks, variables, gammaPerBlock, idBlock, idVar1, nbvV+1);
		}//fin else if
    else if(istLabel=="call"){
        if(CallInst* CI = dyn_cast<CallInst>(inst)){
          if(CI->getCalledFunction()->getName()=="malloc"){
            callDone = 1;
          }
        }
    }
		else if(istLabel=="load"){
  			nameI=inst->getName(); idVar1=variables[nameI]; bl=0;
        operand=inst->getOperand(0);
        Type *T = operand->getType();
        BasicBlock::iterator instTemp = inst;
        instTemp++;
        bool pointerCase = false;
        std::string istLabelNext=instTemp->getOpcodeName(instTemp->getOpcode());
        if((T->isPointerTy() && T->getContainedType(0)->isPointerTy())){
          if((istLabelNext=="store" && instTemp->getOperand(1)->getType()->isPointerTy() &&
          instTemp->getOperand(1)->getType()->getContainedType(0)->isPointerTy())){
            pointerCase = true;
            errs()<<"NOT GOING IN LOAD BECAUSE OF... "<<*inst<<"\n";
          }
        }
        if(!pointerCase){
          int am0 = 0;
          if(operand->getName().find("load") != std::string::npos){// Si v est un pointeur, cela veut dire qu'on le déréférence, on invoque alors PointsToSet
            tempArrayForPtSet.clear();
            A->getPointsToSet(operand,tempArrayForPtSet);
            for(int i = 0; i<tempArrayForPtSet.size(); i++ ){
                newAddAll3D(gammaPerBlock, idBlock, nbvV+1, idVar1, adressToIdVar[tempArrayForPtSet.at(i)]);
            }

            //Si v pointe sur une zone mémoire (AM), alors v est leaké
            for(int i = 0; i<AM.size(); i++){
              if(std::find(tempArrayForPtSet.begin(), tempArrayForPtSet.end(), AM.at(i))!=tempArrayForPtSet.end()){
                am0 = 1;
                break;
              }
            }
            if(am0){
              newAddAll3D(gammaPerBlock,idBlock, nbvV+1, 0, idVar1);
            }
          }
          else if(!(CI = dyn_cast<Constant>(operand)) || isa<GlobalVariable>(operand)){
            nameV=operand->getName();
            idVar2=variables[nameV];
            if(outVar[idVar2]!=1)
              { assign(gammaPerBlock,idBlock, nbvV+1, idVar1, idVar2);}
            else
              { assignOut(gammaPerBlock,idBlock, nbvV+1, idVar1, idVar2); }
          }//fin if cast
           CalculDepBr3D(dependences, idBlocks, variables, gammaPerBlock, idBlock, idVar1, nbvV+1);
        }
		}//fin else if
		else if(istLabel=="br" && opnt_cnt >1){
			operand=inst->getOperand(0);
			nameV=operand->getName();
			idVar1=variables[nameV];
			newAddAll3D(gammaPerBlock,idBlock, nbvV+1, 0, idVar1);//leakage
		}
		errs ()<< "**************************************************************\n";
		//get(*gammaPerBlock[idBlock], *gamma, nbv+1);
		//get(*alphaPerBlock[idBlock], *alpha, nbv+1);
	}//fin for basicblock
		  /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

	if (visited[idBlock]==0)
	  { visited[idBlock]=1;}
	else
	  {
	    join3D2D(gammaPerBlock, idBlock, gammaTemp, nbvV+1);
	      //newAddAllPred3D2D(gammaPerBlock, idBlock, gammaTemp, nbvV+1);
	  }


	errs()<<"Etape : "<<et<<" Old Gamma"<<"\n";
	print2DMp(gammaTemp, inVariables, nbvV+1);
	errs ()<< "**************************************************************\n";


	errs()<<"Etape : "<<et<<" Gamma"<<"["<<idBlock<<"]"<<"\n\n";
	print3Dp(gammaPerBlock, idBlock, inVariables, nbvV+1);
	errs ()<< "**************************************************************\n";

	 /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/


	if (!(equal3D(gammaPerBlock,idBlock, gammaTemp, nbvV+1)) && (visited[idBlock]==1))
	  {
	    errs ()<< "\n *g["<<idBlock<<"] changed !*\n";
	    for(i=1;i<=nbbB;i++){
	      if(sucT[case2T1(idBlock,i,nbbB+1)]==1){
		errs ()<< "*!*add*!*"<<i<<"\n";
		//bb=inIdBlocks[i];
		work.add(i);
	      }
	      else
		{
		  //errs ()<< "\n nothing to add*!*";
		}
	    }
	    //errs ()<< "\n end of what was added*!*";
	  }
  }// fin while

  //errs()<<"gamma"<<"\n\n";
  //print3Dp(gammaPerBlock, nbbB, inVariables, nbvV+1);
  //errs ()<< "**************************************************************\n";
  /*
	errs()<<"dominate"<<"\n\n";
	print2Dp(dominate, nbbB+1);
	errs ()<< "**************************************************************\n";
  */
  /*
	errs()<<"postDominate"<<"\n\n";
	print2Dp(pDominate, nbbB+1);
	errs ()<< "**************************************************************\n";


	errs()<<"dominateur immediat"<<"\n\n";
			for(std::map<int,int>::iterator it=domIm.begin(); it!=domIm.end(); ++it) {
			errs()<<"block "<<it->first<<"\t";
			errs()<< "son dom immediat = "<<it->second<<"\t ";
		}
      		errs()<<"\n\n";


	errs ()<< "**************************************************************\n";

	errs()<<"dep"<<"\n\n";

	for(std::map<int,std::vector<BasicBlock*>>::iterator it=dependences.begin(); it!=dependences.end(); ++it) {
		blockName=inIdBlocks[it->first]->getName();
		errs()<<blockName<<"   depend on :\t";
		vecTemp=it->second;
		for(i=0;i<vecTemp.size();i++){
			errs()<< vecTemp[i]->getName()<<"\t ";
		}
      		errs()<<"\n\n";

	}


		errs()<<"succ"<<"\n\n";

	for(std::map<int,std::vector<BasicBlock*>>::iterator it=succ.begin(); it!=succ.end(); ++it) {
		blockName=inIdBlocks[it->first]->getName();
		errs()<<blockName<<"   has as succesors :\t";
		vecTemp=it->second;
		for(i=0;i<vecTemp.size();i++){
			errs()<< vecTemp[i]->getName()<<"\t ";
		}
      		errs()<<"\n\n";

	}

	errs()<<"blocks"<<"\n\n";
	for(std::map<int, BasicBlock*>::iterator it=inIdBlocks.begin(); it!=inIdBlocks.end(); ++it) {
		blockName=it->second->getName();
		errs()<<blockName<<"\t";
		errs()<< it->first<<"\t";
		}
      		errs()<<"\n\n";


	errs()<<"regionMap"<<"\n\n";
	 	for(std::map<int,std::vector<int>>::iterator it=regionMap.begin(); it!=regionMap.end(); ++it) {
		  blockName=inIdBlocks[it->first]->getName();
		  errs()<<"regionMap of "<<blockName<<" : \t";
		  vecInt=it->second;

		for(i=0;i<vecInt.size();i++){
			errs()<< inIdBlocks[vecInt[i]]->getName() <<"\t ";
		}

      		errs()<<"\n\n";

		}



	errs()<<"suc"<<"\n\n";
	print2Dp(suc, nbbB+1);

	errs()<<"pre"<<"\n\n";
	print2Dp(pre, nbbB+1);
  */
  return 0;

      }
    };

}

char flowSens2::ID = 0;
static RegisterPass<flowSens2> X("fSens2", "calcule de dep.");
