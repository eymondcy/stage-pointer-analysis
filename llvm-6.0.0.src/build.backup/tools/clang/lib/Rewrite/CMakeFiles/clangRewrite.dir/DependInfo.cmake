# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Rewrite/DeltaTree.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/tools/clang/lib/Rewrite/CMakeFiles/clangRewrite.dir/DeltaTree.cpp.o"
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Rewrite/HTMLRewrite.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/tools/clang/lib/Rewrite/CMakeFiles/clangRewrite.dir/HTMLRewrite.cpp.o"
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Rewrite/RewriteRope.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/tools/clang/lib/Rewrite/CMakeFiles/clangRewrite.dir/RewriteRope.cpp.o"
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Rewrite/Rewriter.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/tools/clang/lib/Rewrite/CMakeFiles/clangRewrite.dir/Rewriter.cpp.o"
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Rewrite/TokenRewriter.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/tools/clang/lib/Rewrite/CMakeFiles/clangRewrite.dir/TokenRewriter.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_DEBUG"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "tools/clang/lib/Rewrite"
  "../tools/clang/lib/Rewrite"
  "../tools/clang/include"
  "tools/clang/include"
  "/usr/include/libxml2"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
