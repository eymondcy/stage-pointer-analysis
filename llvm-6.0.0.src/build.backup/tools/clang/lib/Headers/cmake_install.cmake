# Install script for directory: /home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "clang-headers" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/6.0.0/include" TYPE FILE PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ FILES
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/adxintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/altivec.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/ammintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/arm_acle.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/armintr.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/arm64intr.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx2intrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512bwintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512bitalgintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vlbitalgintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512cdintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vpopcntdqintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512dqintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512erintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512fintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512ifmaintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512ifmavlintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512pfintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vbmiintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vbmivlintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vbmi2intrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vlvbmi2intrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vlbwintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vlcdintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vldqintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vlintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vpopcntdqvlintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vnniintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avx512vlvnniintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/avxintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/bmi2intrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/bmiintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/__clang_cuda_builtin_vars.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/__clang_cuda_cmath.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/__clang_cuda_complex_builtins.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/__clang_cuda_intrinsics.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/__clang_cuda_math_forward_declares.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/__clang_cuda_runtime_wrapper.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/cetintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/clzerointrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/cpuid.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/clflushoptintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/clwbintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/emmintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/f16cintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/float.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/fma4intrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/fmaintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/fxsrintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/gfniintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/htmintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/htmxlintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/ia32intrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/immintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/intrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/inttypes.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/iso646.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/limits.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/lwpintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/lzcntintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/mm3dnow.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/mmintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/mm_malloc.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/module.modulemap"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/msa.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/mwaitxintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/nmmintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/opencl-c.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/pkuintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/pmmintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/popcntintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/prfchwintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/rdseedintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/rtmintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/s390intrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/shaintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/smmintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/stdalign.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/stdarg.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/stdatomic.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/stdbool.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/stddef.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/__stddef_max_align_t.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/stdint.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/stdnoreturn.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/tbmintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/tgmath.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/tmmintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/unwind.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/vadefs.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/vaesintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/varargs.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/vecintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/vpclmulqdqintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/wmmintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/__wmmintrin_aes.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/__wmmintrin_pclmul.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/x86intrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/xmmintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/xopintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/xsavecintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/xsaveintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/xsaveoptintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/xsavesintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/xtestintrin.h"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/tools/clang/lib/Headers/arm_neon.h"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "clang-headers" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/6.0.0/include/cuda_wrappers" TYPE FILE PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ FILES
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/cuda_wrappers/algorithm"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/cuda_wrappers/complex"
    "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/tools/clang/lib/Headers/cuda_wrappers/new"
    )
endif()

