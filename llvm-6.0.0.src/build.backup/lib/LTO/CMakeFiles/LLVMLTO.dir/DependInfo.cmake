# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/lib/LTO/Caching.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/lib/LTO/CMakeFiles/LLVMLTO.dir/Caching.cpp.o"
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/lib/LTO/LTO.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/lib/LTO/CMakeFiles/LLVMLTO.dir/LTO.cpp.o"
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/lib/LTO/LTOBackend.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/lib/LTO/CMakeFiles/LLVMLTO.dir/LTOBackend.cpp.o"
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/lib/LTO/LTOCodeGenerator.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/lib/LTO/CMakeFiles/LLVMLTO.dir/LTOCodeGenerator.cpp.o"
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/lib/LTO/LTOModule.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/lib/LTO/CMakeFiles/LLVMLTO.dir/LTOModule.cpp.o"
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/lib/LTO/ThinLTOCodeGenerator.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/lib/LTO/CMakeFiles/LLVMLTO.dir/ThinLTOCodeGenerator.cpp.o"
  "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/lib/LTO/UpdateCompilerUsed.cpp" "/home/cyprien/Documents/Fac/stage-pointer-analysis/llvm-6.0.0.src/build/lib/LTO/CMakeFiles/LLVMLTO.dir/UpdateCompilerUsed.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_DEBUG"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/LTO"
  "../lib/LTO"
  "/usr/include/libxml2"
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
