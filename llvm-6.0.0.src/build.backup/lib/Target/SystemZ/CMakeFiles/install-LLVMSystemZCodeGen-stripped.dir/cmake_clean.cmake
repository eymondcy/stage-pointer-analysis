file(REMOVE_RECURSE
  "SystemZGenAsmMatcher.inc.tmp"
  "SystemZGenAsmMatcher.inc"
  "SystemZGenAsmWriter.inc.tmp"
  "SystemZGenAsmWriter.inc"
  "SystemZGenCallingConv.inc.tmp"
  "SystemZGenCallingConv.inc"
  "SystemZGenDAGISel.inc.tmp"
  "SystemZGenDAGISel.inc"
  "SystemZGenDisassemblerTables.inc.tmp"
  "SystemZGenDisassemblerTables.inc"
  "SystemZGenMCCodeEmitter.inc.tmp"
  "SystemZGenMCCodeEmitter.inc"
  "SystemZGenInstrInfo.inc.tmp"
  "SystemZGenInstrInfo.inc"
  "SystemZGenRegisterInfo.inc.tmp"
  "SystemZGenRegisterInfo.inc"
  "SystemZGenSubtargetInfo.inc.tmp"
  "SystemZGenSubtargetInfo.inc"
  "CMakeFiles/install-LLVMSystemZCodeGen-stripped"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/install-LLVMSystemZCodeGen-stripped.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
