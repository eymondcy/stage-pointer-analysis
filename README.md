# SVF
* [GitHub](https://github.com/SVF-tools/SVF/wiki)
* [Doc Doxygen](https://www.cse.unsw.edu.au/~corg/svf/doxygen/)

# Installation de SVF pour compiler une passe LLVM
* Guide de Victor Baverel qui est tres complet 
* Sinon, le repertoire (zip ou via gitlab) d'où est tiré ce README est déjà 
configuré

# PointerAnalysis
Toutes les analyses de pointeurs héritent de la superclasse **PointerAnalysis**.
Pour faire une analyse, il faut commencer par initialiser l'analyse avec un 
module LLVM (**llvm::Module**) et utiliser la methode `analyze`.

# FlowSensitive
> Header à inclure: *#include "llvm/WPA/FlowSensitive.h*

Pour une analyse flow sensitive, on peut utiliser la classe **FlowSensitive**.
Exemple d'utilisation minimale:

		SVFModule svf_module(M);	// llvm::Module
		bool runOnModule(Module &M) override {
			SVFModule svf_module(M);

			FlowSensitive *fs = FlowSensitive::createFSWPA(svf_module);

			// We do the analysis
			fs->analyze(svf_module);

			// We get the PAG
			PAG* pag = fs->getPAG();

			// Get the constraint graph
			ConstraintGraph* cg = new ConstraintGraph(pag);

			/*
			 * Do something with de PAG or the ConstraintGraph
			 */

			return false;
		}

## PointsTo
Ce que le framework appelle *PointsTo* n'est pas ce que nous entendons par 
PointsTo, a savoir l'information utile de *vers quoi tel pointer pointe*. 
Nous l'avons utilisée dans un premier temps et nous avions, pour chaque variable
du programme leur objet memoire tel qu'il lui a été alloué. Dans le cas des 
*load*, une nouvelle variable est introduite qui elle pointe vers la memoire qui
lui est transmise. L'information telle qu'elle ne nous a pas permis d'avoir les
informations que nous voulions aussi facilement qu'elle semblait le présenter.

## PAG
> Header à inclure: *#include "llvm/MemoryModel/PAG.h*

Le PAG est un pilier de SVF puisqu'il est utilisé dans l'entièreté du framework.
Qu'on utilise une classe d'analyse préfaite ou qu'on en implémente une, on aura 
à se confronter au PAG. Il est très bien expliqué sur [cette page du wiki](https://github.com/svf-tools/SVF/wiki/Technical-documentation). 
Dans l'implémentation technique, les méthodes sont détaillées dans [sa page dans la doc doxygen](https://www.cse.unsw.edu.au/~corg/svf/doxygen/class_p_a_g.html).

Les méthodes communes que j'ai manipulées sont celles qui permettent de
récupérer arcs du graphe ([PAGEdge](https://www.cse.unsw.edu.au/~corg/svf/doxygen/class_p_a_g_edge.html)) et les noeuds ([PAGNode](https://www.cse.unsw.edu.au/~corg/svf/doxygen/class_p_a_g_edge.html)).

Les bouts de code dans la passe permettent de comprendre comment intéragir avec
les arcs et les noeuds. La documentation n'est pas confusante à leur sujet, mais
ça fait un exemple.

## ConstraintGraph
> Header à inclure: *#include "llvm/MemoryModel/ConsG.h*

Le ConstraintGraph est décrit comme étant le PAG avec des arcs supplémentaires 
construit par l'analyse d'Andersen. On peut construire un ConstraintGraph avec 
son constructeur à partir d'un PAG, il va stocker le PAG et le compléter avec de
nouveaux arcs. Comme on peut le voir dans la passe, le **ConstraintGraph** 
construit de cette manière compte peu d'arcs en plus que le PAG. Il est 
difficile de savoir quels sont les arcs que l'on manque, car la classe 
**ConstraintGraph**, a beaucoup d'attributs et de méthodes intéressantes et 
utiles en **private**. Il est plus fastidieux d'intéragir avec le 
**ConstraintGraph** qu'avec le **PAG** quand il s'agit de récupérer des 
informations.

Avec l'exemple du wiki, nous avions inféré un algorithme pour récupérer 
l'information de *PointsTo* qui nous intéresse, à savoir:
1. Choisir un pointeur un **ValueNode** sur le **PAG**/**ConstraintGraph**)
2. Si il y a un arc d'addr, on le remonte pour arriver sur l'**ObjectNode** 
associé
3. On remonte n'importe quel nombre d'arcs de copy (les arcs rajoutés par le 
**ConstraintGraph**)
4. Si un noeud dans lequel on se trouve peut remonter dans un **ObjectNode** 
(par un arc d'Addr), alors ce noeud est dans les PointsTo du noeud initial qu'on
a considéré.

L'exemple que l'on a travaillé ne correspond pas lorsque testé avec SVF en 
local, et l'algorithme devait marcher avec un exemple minimal, mais ne 
fonctionne pas en pratique.

---

SVF permet de créer sa propre analyse, pour ça il faut hériter la nouvelle 
classe de **PointerAnalysis**. Un court exemple est montré [ici](https://github.com/SVF-tools/SVF/wiki/Write-a-flow--and-field---insensitive-pointer-analysis).
Au vu des résultats que l'on a observé au cours de mon stage, il est très 
probable que le moyen le plus efficace de faire une analyse *flow sensitive* 
pour 
produire une structure permettant d'accéder aux PointsTo indexée par les 
instructions du programme est de faire notre propre analyse de cette manière.

# Arborescence

		.
		├── llvm-6.0.0.src
		├── llvm-6.0.0.build
		├── passes
		│   └── PointsTo_pass
		└── tests
			├── chgvar.c
			├── functs.c
			├── github.c
			├── if.c
			├── Makefile
			├── ptr2.c
			├── ptr.c
			└── while.c

# Makefile
## Config
Modifier la variable **TEST** pour changer quel fichier est testé.

La variable **$(LLVM_DIR)** est definie dans mon *.bashrc* comme le répertoire 
de build de LLVM. (Dans mon cas, *{chemin jusqu'au repertoire courant}****/llvm-6.0.0.build/***).

## Commands

* `make`
> Construit les .bc correspondant aux tests definis dans la variable **PROGS**

* `make bitcode`
> Produit les .ll des tests definis dans la variable **PROGS**

* `make run`
> Execute la passe sur l'exemple de test defini dans la variable **TEST**

* `make debug`
> Affiche la commande a executer pour debugger la passe dans gdb, puis la 
> commande a executer pour lancer la passe dans gdb

* `make clean`
> Nettoie les fichiers de compilation

# Lire le code de SVF

Le code de SVF se trouve dans le repertoire *llvm-6.0.0.src/lib/*. Ensuite, pour
le code des classes comme **PAG**, dans le repertoire *MemoryModel* (comme les 
chemins dans les *#include* pour les headers).
La ou les chemins diverges des headers c'est pour les analyses a proprement 
parler (**Andersen**, **FlowSensitive**, etc...) qu'on trouve dans le 
sous-repertoire *Analysis*).

# Compiler la passe
Le guide donné par Laurent Mounier suggère de placer le répertoire *build* dans 
les sources de llvm avec l'arborescence suivante:

		.
		├── llvm-6.0.0.src
		│	├── lib
		│	├── include
		│	├── build
		│	└── ...
		├── passes
		│   └── PointsTo_pass
		└── tests
			├── Makefile
			└── ...

Mais c'est peu facile pour utiliser git et conserver le code source de llvm 
(modifié pour notre installation de SVF). C'est pour cette raison qu'on le 
sépare dans un autre répertoire, ainsi que le code de la passe, pour séparer au 
maximum les différentes parties du code.

Pour compiler llvm, la commande cmake devient donc `cmake -DCMAKE_INSTALL_PREFIX=. ../llvm-6.0.0.src/` 
une fois qu'on est dans le répertoire *llvm-6.0.0.build/*.
Pour compiler les passes, au lieu de mettre le code directement dans le 
répertoire de llvm, je recommande de le mettre dans le répertoire passes et de 
créer un lien symbolique vers lui jusqu'à LLVM.

# Documentation

Le code est assez largement commenté, il est possible de générer une doc doxygen
a partir des sources de la passe, le fichier de configuration est déjà présent 
dans *passes/PointsTo_pass/doxyfile*.
