/**
 * \file PointsTo_pass.cpp
 * \brief	File that contains the main code for the LLVM pass
 * \author	Cyprien Eymond Laritaz
 */

#include <vector>
#include <sstream>
#include <queue>
#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Support/raw_ostream.h"

/* SVF libs */
#include "llvm/MemoryModel/PAG.h"
#include "llvm/MemoryModel/ConsG.h"
#include "llvm/Util/AnalysisUtil.h"
#include "llvm/WPA/FlowSensitive.h"

// Macro to prevent the compiler from complaining about unsed variables
#define UNUSED(x) ((void)(x));

using namespace llvm;
using namespace analysisUtil;

typedef std::vector<NodeID> NodeIDset;

/**
 * \brief	Check if a vector contains an element or not
 * \param	vec Vector of elements
 * \param	val	Value to search
 * \return	`true` if the element *val* is found in *vec*, `false` otherwise.
 */
template<class T>
bool contains(std::vector<T> vec, T val) {
	return std::find(vec.begin(), vec.end(), val) != vec.end();
}

/**
 * \brief	Converts a `NodeID` to a `std::string` which is his name in the 
 * 			program assignment graph passed as parameter
 * \param	node	ID of the node we want to print in the pag
 * \param	pag		Program assignment graph that holds the names of the nodes
 * \return	The name of the variable (or the name of the pointer that alloc'ed
 * 			it if it's an Object node)
 */
std::string id_to_name(NodeID node, PAG* pag) {
	if(pag->getPAGNode(node)->hasValue())
		return pag->getPAGNode(node)->getValue()->getName();
	else
		return "No Name";
}

/**
 * \brief	Prints an edge of a SVF graph
 * \param	edge	Generic edge that works either for a PAGEdge or a 
 * 					ConstraintEdge.
 * \param	pag		Program assignment graph that holds the information about 
 * 					the name of the nodes. Equal to `NULL` if no 2nd parameter 
 * 					is provided, it implies no printout of the names of the 
 * 					nodes.
 */
template <typename T>
void print_edge(GenericEdge<T>* edge, PAG* pag = NULL) {
	NodeID from = edge->getSrcID(), to = edge->getDstID();
	errs() << "\tfrom " << from;
	if(pag != NULL)
		errs() << " (" << id_to_name(from, pag) << ")";
	errs() << " to " << to;
	if(pag != NULL)
		errs() << " (" << id_to_name(to, pag) << ")";
	errs() << "\n";
}

/**
 * \brief	Prints the data of a PAG
 * \param	pag	Pointer to the graph to print
 */
void print_PAG(PAG* pag) {
	errs() << "PRINT PAG\n";
	errs() << "Addr edges:\n";
	for(auto edge : pag->getEdgeSet(PAGEdge::PEDGEK::Addr))
		print_edge(edge, pag);
	errs() << "Copy edges:\n";
	for(auto edge : pag->getEdgeSet(PAGEdge::PEDGEK::Copy))
		print_edge(edge, pag);
	errs() << "Store edges:\n";
	for(auto edge : pag->getEdgeSet(PAGEdge::PEDGEK::Store))
		print_edge(edge, pag);
	errs() << "Load edges:\n";
	for(auto edge : pag->getEdgeSet(PAGEdge::PEDGEK::Load))
		print_edge(edge, pag);
	errs() << "Call edges:\n";
	for(auto edge : pag->getEdgeSet(PAGEdge::PEDGEK::Call))
		print_edge(edge, pag);
	errs() << "Ret edges:\n";
	for(auto edge : pag->getEdgeSet(PAGEdge::PEDGEK::Ret))
		print_edge(edge, pag);
	errs() << "NormalGep edges:\n";
	for(auto edge : pag->getEdgeSet(PAGEdge::PEDGEK::NormalGep))
		print_edge(edge, pag);
	errs() << "VariantGep edges:\n";
	for(auto edge : pag->getEdgeSet(PAGEdge::PEDGEK::VariantGep))
		print_edge(edge, pag);
	errs() << "ThreadFork edges:\n";
	for(auto edge : pag->getEdgeSet(PAGEdge::PEDGEK::ThreadFork))
		print_edge(edge, pag);
	errs() << "ThreadJoin edges:\n";
	for(auto edge : pag->getEdgeSet(PAGEdge::PEDGEK::ThreadJoin))
		print_edge(edge, pag);
}

/**
 * \brief	Prints the data of a ConstraintGraph
 * \param	cg	Pointer to the graph to print
 * \param	pag	Pointer to the PAG that holds the name of the nodes. By default,
 * 			equal to `NULL` means that if you don't provide a pag, no name will 
 * 			be displayed, only the nodes IDs.
 */
void print_CG(ConstraintGraph* cg, PAG* pag = NULL) {
	errs() << "PRINT CG\n";
	errs() << "Addr edges:\n";
	for(auto edge : cg->getAddrCGEdges())
		print_edge(edge, pag);
	errs() << "Copy edges:\n";
	for(auto edge : cg->getDirectCGEdges())
		if(edge->getEdgeKind() == ConstraintEdge::ConstraintEdgeK::Copy)
			print_edge(edge, pag);
	errs() << "Store edges:\n";
	for(auto edge : cg->getDirectCGEdges())
		if(edge->getEdgeKind() == ConstraintEdge::ConstraintEdgeK::Store)
			print_edge(edge, pag);
	errs() << "Load edges:\n";
	for(auto edge : cg->getDirectCGEdges())
		if(edge->getEdgeKind() == ConstraintEdge::ConstraintEdgeK::Load)
			print_edge(edge, pag);
	errs() << "NormalGep edges:\n";
	for(auto edge : cg->getDirectCGEdges())
		if(edge->getEdgeKind() == ConstraintEdge::ConstraintEdgeK::NormalGep)
			print_edge(edge, pag);
	errs() << "VariantGep edges:\n";
	for(auto edge : cg->getDirectCGEdges())
		if(edge->getEdgeKind() == ConstraintEdge::ConstraintEdgeK::VariantGep)
			print_edge(edge, pag);
}

/**
 * \brief	Retrieve a list of `llvm::Value` corresponding to the the variables 
 * 			of the program (including temporary variables)
 * \param	M 	Module from which we take the variables
 * \param	pag	Pogram Assignment Graph corresponding to the module, used to 
 * 			print the `NodeID` of each variable (for debugging purpose only)
 * \return	A vector of `llvm::Value` that is the variables of the module
 */
std::vector<Value*> retrieve_variables(llvm::Module& M, PAG* pag) {
	std::vector<Value*> variables;
	int temp = 1;

	for(auto func = M.begin(); func != M.end(); func++) {
		errs() << "From function " << func->getName() << ":\n";
		for(auto block = func->begin(); block != func->end(); block++) {
			for(auto inst = block->begin(); inst != block->end(); inst++) {
				string opname = inst->getOpcodeName(inst->getOpcode());
				if(inst->isBinaryOp() || opname == "alloca" || opname == "load"
				|| opname == "zext" || opname == "sext" || opname == "bitcast"
				|| opname == "phi" || opname == "getelementptr" 
				|| opname == "trunc" || opname == "icmp") {
					if(!inst->hasName()) {
						std::ostringstream oss(std::ostringstream::ate);
						oss << "%" << temp;
						inst->setName(oss.str());
						++temp;
					}
					if(!contains(variables, inst->stripPointerCasts()))  {
						errs() << "\tVariable " << inst->getName() << " (";
						errs() << opname << ") [";
						errs() << pag->getValueNode(inst->stripPointerCasts());
						errs() << "]\n";
						variables.push_back(inst->stripPointerCasts());
					}
				}
			}
		}
		errs() << "\n";
	}
	return variables;
}

/**
 * \brief	Follow an address edge from the destination to the origin
 * \param	node	ID of the node of which we want to recover the source of its
 * 					address edges.
 * \param	cg		`ConstraintGraph` that contains the informations of the 
 * 					nodes and edges.
 * \return	A vector of `NodeID` that are the sources of each addr edges leading
 * 			to the node of ID *node*
 */
NodeIDset follow_address_edge(NodeID node, ConstraintGraph* cg) {
	ConstraintNode *cgnode = cg->getConstraintNode(node);
	NodeIDset nodeset;
	for(auto edge : cgnode->incomingAddrEdges()) {
		if(!contains(nodeset, edge->getSrcID()))
			nodeset.push_back(edge->getSrcID());
	}
	return nodeset;
}

/**
 * \brief	Follow an copy edge from the destination to the origin
 * \param	node	ID of the node of which we want to recover the source of its
 * 					copy edges.
 * \param	cg		`ConstraintGraph` that contains the informations of the 
 * 					nodes and edges.
 * \return	A vector of `NodeID` that are the sources of each copy edges leading
 * 			to the node of ID *node*
 */
NodeIDset follow_copy_edge(NodeID node, ConstraintGraph* cg) {
	NodeIDset nodeset;
	for(auto edge : cg->getDirectCGEdges()) {
		if(edge->getEdgeKind() == ConstraintEdge::ConstraintEdgeK::Copy && edge->getDstID() == node && !contains(nodeset, edge->getSrcID()))
			nodeset.push_back(edge->getSrcID());
	}
	return nodeset;
}

/**
 * \brief	Get the PointTo information of a variable
 * \param	node	`NodeID` of the variable we want to get the PointsTo 
 * 					informations in the `ConstraintGraph`.
 * \param	cg		`ConstraintGraph` that holds the dependency of each pointer.
 * \return	A vector of `NodeID` corresponding to the nodes that the pointer of 
 * 			ID *node* points to.
 * 
 * To recover the information, we need to follow an incoming address edge to the
 * node we consider, and then get the memory object node. Then, all the Value 
 * nodes (pointers) that are reachable by reversely following the direct copy 
 * edges. This algorithm was found by analysing [this example](https://github.co
 * m/SVF-tools/SVF/wiki/Analyze-a-Simple-C-Program), but doesn't work in 
 * practice (the example may be too minimal to let us find a generic algorithm 
 */
NodeIDset getPointsTo(NodeID node, ConstraintGraph* cg) {
	NodeIDset nodeset;
	std::queue<NodeID> worklist;

	// Follow the first addr edge
	for(auto addr : follow_address_edge(node, cg))
		worklist.push(addr);

	while(!worklist.empty()) {
		NodeID node = worklist.front();
		worklist.pop();

		// We follow copy edges
		for(auto copy : follow_copy_edge(node, cg))
			worklist.push(copy);

		// We add all the value nodes that contain an incoming addr edge
		for(auto addr : follow_address_edge(node, cg)) {
			nodeset.push_back(node);
			
			UNUSED(addr) // Prevent the compiler from complaining about unused 
			             // variable addr
		}
	}
	return nodeset;
}

namespace {
/**
 * \class	PointsTo_pass
 * \brief	Pointer analysis pass
 */
class PointsTo_pass : public ModulePass {
public:
	static char ID;

	PointsTo_pass() : ModulePass(ID) {}

	/**
	 * \brief	Code executed on the modules of the program. Basically our main 
	 * 			function
	 * \param	M	A reference to the module to analyse
	 */
	bool runOnModule(Module &M) override {
		SVFModule svf_module(M);

		FlowSensitive *fs = FlowSensitive::createFSWPA(svf_module);
		
		// We do the analysis
		fs->analyze(svf_module);
		
		// We get the PAG
		PAG* pag = fs->getPAG();

		// Get the constraint graph
		ConstraintGraph* cg = new ConstraintGraph(pag);

		// Get the variables list
		std::vector<llvm::Value*> variables = retrieve_variables(M, pag);

		// Print the pointsto of each variable
		for(auto val : variables) {
			NodeID node = pag->getValueNode(val);
			// We get the pointsto
			errs() << "PointsTo of " << val->getName() << ": ";
			for(auto ptstodata : getPointsTo(node, cg)) {
				PAGNode *ptnode = pag->getPAGNode(ptstodata);
				errs() <<  ptnode->getValue()->getName() << " ";
			}
			errs() << "\n";
		}
		errs() << "\n";

		// We print the PAG and the ConstraintGraph for debug purpose 
		print_PAG(pag);
		errs() << "\n";
		print_CG(cg, pag);
		return false;
	}
}; // end of class PointsTo
}  // end of anonymous namespace

char PointsTo_pass::ID = 0;
static RegisterPass<PointsTo_pass> X("pointsto", "PointsTo Pass", false, false);
